package excelOkuma;

import java.util.Comparator;

public class Ogrenci {

    String isim;
    String numara;
    Sinif sinavSalonu;
    String sinif;

    int siraNo;

    boolean kaldiMi;

    String basHarfler;

    public Ogrenci(String isim, String numara) {
        super();
        this.isim = isim.toUpperCase();
        this.numara = numara;

    }

    public Ogrenci(String isim, String numara, String bas) {
        super();
        this.isim = isim.toUpperCase();
        this.numara = numara;
        this.basHarfler = bas;
    }

    public Ogrenci(String isim, String numara, boolean kaldiMi) {
        super();
        this.isim = isim.toUpperCase();
        this.numara = numara;
        this.kaldiMi = kaldiMi;
    }

    public Ogrenci(String isim, String numara, String sinif, int siraNo, String basHarfler) {
        this.isim = isim;
        this.numara = numara;
        this.sinif = sinif;
        this.siraNo = siraNo;
        this.basHarfler = basHarfler;
    }

    public static Comparator<Ogrenci> OgrenciNoComparator = new Comparator<Ogrenci>() {

        @Override
        public int compare(Ogrenci o1, Ogrenci o2) {
            String ogNo1 = o1.getNumara().toUpperCase();
            String ogNo2 = o2.getNumara().toUpperCase();

            //ascending order
            return ogNo1.compareTo(ogNo2);
        }
    };

    public static Comparator<Ogrenci> OgrenciSinifComparator = new Comparator<Ogrenci>() {

        @Override
        public int compare(Ogrenci o1, Ogrenci o2) {
            String ogSinif1 = o1.getSinif();
            String ogSinif2 = o2.getSinif();

            //ascending order
            return ogSinif1.compareTo(ogSinif2);
        }
    };

    public static Comparator<Ogrenci> OgrenciSinifOgrenciComparator = new Comparator<Ogrenci>() {

        @Override
        public int compare(Ogrenci o1, Ogrenci o2) {
            String ogSinif1 = o1.getSinif();
            String ogSinif2 = o2.getSinif();

            int sComp = ogSinif1.compareTo(ogSinif2);

            if (sComp != 0) {
                return sComp;
            }

            Integer x1 = ((Ogrenci) o1).getSiraNo();
            Integer x2 = ((Ogrenci) o2).getSiraNo();
            return x1.compareTo(x2);
            //ascending order
            //return ogSinif1.compareTo(ogSinif2);
        }
    };

    public int getSiraNo() {
        return siraNo;
    }

    public void setSiraNo(int siraNo) {
        this.siraNo = siraNo;
    }

    public String getBasHarfler() {
        return basHarfler;
    }

    public void setBasHarfler(String basHarfler) {
        this.basHarfler = basHarfler;
    }

    public String getIsim() {
        return isim.toUpperCase();
    }

    public void setIsim(String isim) {
        this.isim = isim.toUpperCase();
    }

    public String getNumara() {
        return numara;
    }

    public void setNumara(String numara) {
        this.numara = numara;
    }

    public boolean isKaldiMi() {
        return kaldiMi;
    }

    public void setKaldiMi(boolean kaldiMi) {
        this.kaldiMi = kaldiMi;
    }

    public Sinif getSinavSalonu() {
        return sinavSalonu;
    }

    public void setSinavSalonu(Sinif sinavSalonu) {
        this.sinavSalonu = sinavSalonu;
    }

    public String getSinif() {
        return sinavSalonu.getSinifAdi();
    }

    @Override
    public String toString() {
        return "Ogrenci [isim=" + isim + ", numara=" + numara + "]";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author gurcan
 */
public class AnaGuiController implements Initializable {

    @FXML
    private Button listeEkleButonu;
    @FXML
    private Button listeCikarButonu;
    @FXML
    private Button listeTemizleButonu;
    @FXML
    private Button sinifEkleButonu;
    @FXML
    private Button sinifCikarButonu;
    @FXML
    private Button sinifTemizleButonu;
    @FXML
    private ComboBox<?> sinavDonemi;
    @FXML
    private ComboBox<?> sinavSaati;
    @FXML
    private Button tutanakUretButon;
    @FXML
    private Button listeUretButon;
    @FXML
    private Label sinifKapasiteEtiket;
    @FXML
    private Label ogrenciKapasiteEtiket;
    @FXML
    private ListView<?> listeler;
    @FXML
    private ListView<?> sinifListe;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void listeEkleButonuAction(ActionEvent event) {
        listeEkle();
    }

    @FXML
    private void listeCikarButonuAction(ActionEvent event) {
    }

    @FXML
    private void listeTemizleButonuAction(ActionEvent event) {
    }

    @FXML
    private void sinifEkleButonuAction(ActionEvent event) {

    }

    @FXML
    private void sinifCikarButonuAction(ActionEvent event) {
    }

    @FXML
    private void sinifTemizleButonuAction(ActionEvent event) {
    }

    @FXML
    private void tutanakUretButonAction(ActionEvent event) {
    }

    @FXML
    private void listeUretButonAction(ActionEvent event) {
    }



}

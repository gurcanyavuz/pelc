/*
    email   :    gurcanyavuz[at]gmail.com
    date    :    03.Oca.2019
    project :    Sinav
 */
package excelOkuma;

/**
 *
 * @author gurcan
 */
public class Sinav {

    String dersAdi;
    String bolumAdi;
    String seansSaati;
    String seansAdi;
    String sinavTarihi;
    String listeSekli;

    public Sinav(String dersAdi, String bolumAdi, String seansSaati, String seansAdi, String sinavTarihi, String listeSekli) {
        this.dersAdi = dersAdi;
        this.bolumAdi = bolumAdi;
        this.seansSaati = seansSaati;
        this.seansAdi = seansAdi;
        this.sinavTarihi = sinavTarihi;
        this.listeSekli = listeSekli;
    }

    public Sinav() {
    }

    public String getDersAdi() {
        return dersAdi;
    }

    public void setDersAdi(String dersAdi) {
        this.dersAdi = dersAdi;
    }

    public String getBolumAdi() {
        return bolumAdi;
    }

    public void setBolumAdi(String bolumAdi) {
        this.bolumAdi = bolumAdi;
    }

    public String getSeansSaati() {
        return seansSaati;
    }

    public void setSeansSaati(String seansSaati) {
        this.seansSaati = seansSaati;
    }

    public String getSeansAdi() {
        return seansAdi;
    }

    public void setSeansAdi(String seansAdi) {
        this.seansAdi = seansAdi;
    }

    public String getSinavTarihi() {
        return sinavTarihi;
    }

    public void setSinavTarihi(String sinavTarihi) {
        this.sinavTarihi = sinavTarihi;
    }

    public String getListeSekli() {
        return listeSekli;
    }

    public void setListeSekli(String listeSekli) {
        this.listeSekli = listeSekli;
    }

}
